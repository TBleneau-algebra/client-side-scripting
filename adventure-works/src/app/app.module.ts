/**
 * Import of Angular's modules
 */
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
/**
 * Import of Nebular's modules
 */
import {NbDatepickerModule, NbDialogModule, NbMenuModule, NbSidebarModule, NbToastrModule, NbWindowModule} from '@nebular/theme';
import {NbAuthModule} from '@nebular/auth';
import {NbSecurityModule} from '@nebular/security';
/**
 * Import of application's modules
 */
import {ThemeModule} from './shared/theme/theme.module';
import {AppRoutingModule} from './app-routing.module';
import {ComponentsModule} from './shared/components/components.module';
/**
 * Import of application's components
 */
import {AppComponent} from './app.component';
/**
 * Import of application's services
 */
import {HttpInterceptorService} from './shared/services/http-interceptor/http-interceptor.service';
import {CacheService} from './shared/services/cache/cache.service';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        ComponentsModule,
        ThemeModule.forRoot(),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbDatepickerModule.forRoot(),
        NbDialogModule.forRoot(),
        NbWindowModule.forRoot(),
        NbToastrModule.forRoot(),
        NbAuthModule.forRoot(),
        NbSecurityModule.forRoot(),
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true},
        CacheService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
