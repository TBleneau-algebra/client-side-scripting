import {NbMenuItem} from '@nebular/theme';

/**
 * @description This array represents the definition of the menu in the application
 */
export const menuData: Array<NbMenuItem> = [
    {
        title: 'Management',
        group: true
    },
    {
        title: 'Customers',
        icon: 'people-outline',
        link: '/pages/customer'
    },
    {
        title: 'Bills',
        icon: 'file-text-outline',
        link: '/pages/bill',
        children: [
            {
                title: 'List of Bills',
                icon: 'list-outline',
                link: '/pages/bill/list',
            },
            {
                title: 'Create a new Bill',
                icon: 'file-add-outline',
                link: '/pages/bill/create',
            }
        ]
    },
    {
        title: 'Products',
        icon: 'shopping-cart-outline',
        link: '/pages/product'
    },
];

/**
 * @description This array represents the definition of the mobile menu in the application
 */
export const menuMobileData: Array<NbMenuItem> = [
    {
        title: 'Management',
        group: true
    },
    {
        title: 'Customers',
        icon: 'people-outline',
        link: '/pages/customer'
    },
    {
        title: 'Bills',
        icon: 'file-text-outline',
        link: '/pages/bill',
        children: [
            {
                title: 'List of Bills',
                icon: 'list-outline',
                link: '/pages/bill/list',
            },
            {
                title: 'Create a new Bill',
                icon: 'file-add-outline',
                link: '/pages/bill/create',
            }
        ]
    },
    {
        title: 'Products',
        icon: 'shopping-cart-outline',
        link: '/pages/product'
    },
    {
        title: 'User Account',
        group: true
    },
    {
        title: 'Profile',
        icon: 'person-outline',
        link: '/pages/user',
        queryParams: {
            username: null
        }
    },
    {
        title: 'Login',
        icon: 'log-in-outline',
        link: '/authentication/login'
    },
    {
        title: 'Logout',
        icon: 'log-out-outline',
        link: '/authentication/logout'
    },
];
