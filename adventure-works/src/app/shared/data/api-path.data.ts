export const GET_CUSTOMER_BY_ID: string = '/customer/';

export const GET_CUSTOMER_BY_BILL_ID: string = '/customerByBill/';

export const GET_CUSTOMERS: string = '/customers';

export const GET_STATES: string = '/states';

export const GET_CITIES: string = '/cities';

export const GET_CITIES_BY_STATE_ID: string = '/cities/';

export const GET_CUSTOMER_BILL_BY_CUSTOMER_ID: string = '/customerbills/';

export const GET_BILL_ITEM_BY_BILL_ID: string = '/billitems/';

export const GET_SELLERS: string = '/sellers/';

export const GET_CATEGORIES: string = '/categories/';

export const GET_SUBCATEGORIES_BY_CATEGORY_ID: string = '/subcategories/';

export const GET_PRODUCTS_BY_SUBCATEGORY_ID: string = '/products/';

export const POST_ADD_CUSTOMER: string = '/addcustomer';

export const POST_EDIT_CUSTOMER: string = '/editcustomer';

export const POST_DELETE_CUSTOMER: string = '/deletecustomer';

export const POST_ADD_CREDIT_CARD: string = '/addcreditcard';

export const POST_ADD_BILL: string = '/addbill';

export const POST_DELETE_BILL: string = '/deleteBill';

export const POST_ADD_ITEM: string = '/additem';

export const POST_DELETE_ITEM: string = '/deleteItem';

export const POST_RETRIEVE_CUSTOMER: string = '/getCustomer';

export const POST_RETRIEVE_USER: string = '/getUser';

export const POST_EDIT_USER: string = '/editUser';

export const POST_REGISTER_USER: string = '/registeruser';

export const POST_LOGIN_USER: string = '/login';
