import {Injectable} from '@angular/core';

@Injectable()
export class CacheService {

    /**
     * @description Constructor of CacheService service
     *
     * The constructor creates an instance of the CacheService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
    }

    /**
     * @description This method allows you to save a cached item in the current session of the application
     *
     * @param key Key to the item to be saved
     * @param data Data of the item to be backed up
     */
    save(key: string, data: any): void {
        if (typeof data === 'string') {
            sessionStorage.setItem(key, data);
        } else {
            sessionStorage.setItem(key, JSON.stringify(data));
        }
    }

    /**
     * @description This method is used to load a cached item in the current session of the application
     *
     * @param key Key to the element to be recovered
     */
    load(key: string): any {
        const item: string = sessionStorage.getItem(key);

        if (item) {
            return JSON.parse(item);
        }
        return null;
    }
}

