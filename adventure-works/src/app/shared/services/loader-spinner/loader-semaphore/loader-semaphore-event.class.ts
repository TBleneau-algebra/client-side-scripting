import {Subject} from 'rxjs';

/**
 * @description This class represents a simple definition for a simple event management
 */
export class LoaderSemaphoreEventClass<T> {

  /**
   * @description A callback to call when the event is throw
   */
  public callable: (val: T) => void = null;

  /**
   * @description A subject to listen when the event is throw
   */
  public subject: Subject<T> = new Subject<T>();
}


