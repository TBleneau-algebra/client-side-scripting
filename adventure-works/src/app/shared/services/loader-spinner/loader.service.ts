import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CustomSubjectClass} from '../../classes/rxjs/custom-subject.class';
import {LoaderSemaphoreClass} from './loader-semaphore/loader-semaphore.class.';


@Injectable({
    providedIn: 'root',
})
export class LoaderService {

    /**
     * @description List of available loader spinner
     */
    private availableLoaders: { [key: string]: { subject: CustomSubjectClass<boolean>, current: number } } = {};

    /**
     * @description Constructor of LoaderService service
     *
     * The constructor creates an instance of the LoaderService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
    }

    /**
     * @description This method add the loader tagged with 'loaderId' to the internal list of loaders of loaderService
     *
     * @param loaderId Loader spinner identifier
     * @param initialState state which defines if the loader spinner is activated or not
     */
    public register(loaderId: string, initialState: boolean = false): LoaderService {
        if (this.availableLoaders[loaderId]) {
            return this;
        }
        this.availableLoaders[loaderId] = {
            subject: new CustomSubjectClass<boolean>(),
            current: initialState ? 1 : 0,
        };
        return this;
    }

    /**
     * @description This method remove the loader tagged with 'loaderId' to the internal list of loaders of loaderService
     *
     * @param loaderId Loader spinner identifier
     */
    public unregister(loaderId: string): LoaderService {
        if (!this.availableLoaders[loaderId]) {
            return this;
        }
        this.availableLoaders[loaderId] = undefined;
    }

    /**
     * @description This method returns an event listener for the loader tagged with 'loaderId'
     *
     * @param loaderId Loader spinner identifier
     */
    public getObservable(loaderId: string): Observable<boolean> {
        return this.availableLoaders[loaderId].subject.asObservable();
    }

    /**
     * @description This method throw an event to make the loader registered with 'loaderId' active
     *
     * @param loaderId Loader spinner identifier
     */
    public load(loaderId: string): boolean {
        let loader = this.availableLoaders[loaderId] || null;

        if (loader) {
            loader.subject.next(true);
            ++loader.current;
        } else {
            loader = {subject: new CustomSubjectClass<boolean>(), current: 1};
            this.availableLoaders[loaderId] = loader;
        }
        return true;
    }

    /**
     * @description This method allows you to attach a semaphore to a loader spinner
     *
     * @param loaderId Loader spinner identifier
     * @param semaphore LoaderSemaphoreClass used by the loader service for the current loader spinner (defined by the loaderId)
     */
    public attachSemaphore(loaderId: string, semaphore: LoaderSemaphoreClass) {
        semaphore.release.subscribe(value => {
            if (value) {
                this.unload(loaderId);
            }
        });
    }

    /**
     * @description This method throw an event to make the loader registered with 'loaderId' inactive.
     *
     * @param loaderId Loader spinner identifier
     */
    public unload(loaderId: string): boolean {
        const loader = this.availableLoaders[loaderId] || null;

        if (loader && loader.current > 0) {
            loader.subject.next(false);
            --loader.current;
        }
        return true;
    }
}
