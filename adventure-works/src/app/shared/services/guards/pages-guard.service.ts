import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RequestService} from '../request/request.service';
import {GET_CUSTOMER_BY_BILL_ID, GET_CUSTOMER_BY_ID} from '../../data/api-path.data';

@Injectable({
    providedIn: 'root'
})
export class PagesGuardService implements CanActivate {

    /**
     * @description Constructor of PagesGuardService service
     *
     * The constructor creates an instance of the PagesGuardService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param requestService A reference to the http request service in the application
     */
    constructor(private router: Router, private requestService: RequestService) {
    }

    /**
     *  @description This method is a guard and decides if a route can be activated
     *  If all guards return true, navigation will continue.
     *  If any guard returns false, navigation will be cancelled.
     *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
     *  returned from the guard.
     *
     * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
     * @param state represents the state of the router at a moment in time.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
        boolean | UrlTree {
        return new Promise(resolve => {
            const redirect: string = (state.url.slice(0, 11) === '/pages/bill') ? '/pages/bill' : '/pages/customer';
            const path: string = (redirect === '/pages/bill') ? GET_CUSTOMER_BY_BILL_ID : GET_CUSTOMER_BY_ID;

            if (route.paramMap.get('id')) {
                this.requestService.get(path + route.paramMap.get('id'))
                    .then(response => {
                        if (response.responseData !== null) {
                            resolve(true);
                        } else {
                            resolve(this.router.parseUrl(redirect));
                        }
                    })
                    .catch(() => resolve(this.router.parseUrl(redirect)));
            } else {
                resolve(this.router.parseUrl(redirect));
            }
        });
    }
}
