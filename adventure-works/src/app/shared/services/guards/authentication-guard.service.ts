import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserRequestService} from '../request/user-request.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

    /**
     * @description Constructor of AuthenticationGuardService service
     *
     * The constructor creates an instance of the AuthenticationGuardService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param userRequestService A reference to the http request service used for user information in the application
     */
    constructor(private router: Router, private userRequestService: UserRequestService) {
    }

    /**
     *  @description This method is a guard and decides if a route can be activated
     *  If all guards return true, navigation will continue.
     *  If any guard returns false, navigation will be cancelled.
     *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
     *  returned from the guard.
     *
     * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
     * @param state represents the state of the router at a moment in time.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
        boolean | UrlTree {
        return new Promise(resolve => {
            if (state.url.slice(0, 21) === '/authentication/login' || state.url.slice(0, 24) === '/authentication/register') {
                this.userRequestService.expiredToken() ? resolve(true) : resolve(this.router.parseUrl('/pages'));
            } else {
                (window.innerWidth < 728 && !this.userRequestService.expiredToken()) ? resolve(true) :
                    resolve(this.router.parseUrl('/pages'));

            }
        });
    }
}
