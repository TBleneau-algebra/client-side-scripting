import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserRequestService} from '../request/user-request.service';
import {UserClass} from '../../classes/models/user.class';
import {CacheService} from '../cache/cache.service';

@Injectable({
    providedIn: 'root'
})
export class UserGuardService implements CanActivate {

    /**
     * @description Constructor of UserGuardService service
     *
     * The constructor creates an instance of the UserGuardService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param userRequestService A reference to the http request service used for user information in the application
     * @param cacheService A reference to the cache service of the application
     */
    constructor(private router: Router, private userRequestService: UserRequestService, private cacheService: CacheService) {
    }

    /**
     *  @description This method is a guard and decides if a route can be activated
     *  If all guards return true, navigation will continue.
     *  If any guard returns false, navigation will be cancelled.
     *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
     *  returned from the guard.
     *
     * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
     * @param state represents the state of the router at a moment in time.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
        boolean | UrlTree {
        return new Promise(resolve => {
            const username: string = route.queryParamMap.get('username');

            if (this.userRequestService.expiredToken() || username === null) {
                resolve(this.router.parseUrl('/pages'));
            } else {
                const user: UserClass = this.cacheService.load('user');

                if (user) {
                    (user.username === username) ? resolve(true) : resolve(this.router.parseUrl('/pages'));
                } else {
                    resolve(this.router.parseUrl('/pages'));
                }

            }
        });
    }
}
