import {RequestParamsInterface} from './request-params.interface';

/**
 * @description RequestParamsListClass is a class that defines the different properties that can be reached
 * to an Http request using Angular's HttpClient service
 */
export class RequestParamsListClass implements RequestParamsInterface {

    /**
     * @description The body of the answer does not return all the data that may be required.
     * Simply specify to Angular's HttpClient service that you want a complete answer with the option
     * "observe
     *
     * Available values 'response' | 'events' | ...
     */
    observe: any;

    /**
     * @description The property defines whether the request should be made in such a way as to expose the events
     * of progress
     */
    reportProgress: boolean;

    /**
     * @description The property defines the type of response expected from the server.
     *
     * Available values 'arraybuffer' | 'blob' | 'json' | 'text'
     */
    responseType: any;

    /**
     * @description The property defines whether the request should be sent with the identification information
     * outgoing (cookies)
     */
    withCredentials: boolean;

    /**
     * @description Outgoing URL settings
     */
    params: Array<{ name: string, value: any }>;

    /**
     * @description Outgoing headers for the executed request
     */
    headers: Array<{ name: string, value: string }>;

    /**
     * @description Constructor of the class RequestParamsClass
     */
    constructor(public pageFrom: string, public pageSize: string) {
        this.params = [];
        this.headers = [];

        this.headers.push(
            {
                name: 'page.from',
                value: (pageFrom !== null) ? pageFrom : '0'
            },
            {
                name: 'page.size',
                value: (pageSize !== null) ? pageSize : '20'
            });

        this.observe = 'response';
        this.responseType = 'json';
        this.reportProgress = false;
        this.withCredentials = false;
    }
}
