import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {RequestClass} from './request.class';
import {LoaderService} from '../loader-spinner/loader.service';
import {environment} from '../../../../environments/environment';
import {JwTokenClass} from '../../classes/models/jw-token.class';
import {CustomSubjectClass} from '../../classes/rxjs/custom-subject.class';
import {UserClass} from '../../classes/models/user.class';
import {POST_RETRIEVE_USER} from '../../data/api-path.data';

@Injectable({
    providedIn: 'root'
})
export class UserRequestService extends RequestClass {

    /**
     * @description Listener to know if the user is connected
     *
     * A Subject is a special type of Observable that allows values to be multicasted to many Observers.
     * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
     *
     * Subject are multicast.
     */
    private _connectedObservable: CustomSubjectClass<boolean>;

    /**
     * @description Listener and sender to retrieve user model data (when a user is connected)
     *
     * A Subject is a special type of Observable that allows values to be multicasted to many Observers.
     * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
     *
     * Subject are multicast.
     */
    private _userObservable: CustomSubjectClass<UserClass>;

    /**
     * @description Constructor of UserRequestService service
     *
     * The constructor creates an instance of the UserRequestService service and specifies the default values
     * the input and output variables of the service.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param loaderService A reference to the loader service of the application
     */
    constructor(httpClient: HttpClient, location: Location, loaderService: LoaderService) {
        super(httpClient, location, environment.apiHost, loaderService);

        this.userObservable = new CustomSubjectClass<UserClass>();
        this.connectedObservable = new CustomSubjectClass<boolean>();
    }

    /**
     * @description This method saves the data of the user logged in in the current session
     *
     * @param value Username of the connected user
     */
    insertedUser(value: string): void {
        let user: UserClass = new UserClass();

        user.username = value;
        this.post(user, POST_RETRIEVE_USER)
            .then(response => {
                user = response.responseData;
                user.img = (user.img) ? user.img : '/assets/img/user.png';

                sessionStorage.setItem('user', JSON.stringify(user));
                this.userObservable.next(user);
            })
            .catch(() => this.userObservable.next(null));
    }

    /**
     * @description This method allows to save the user authentication token in the current session in order to provide it
     * for each request to the API
     *
     * @param value Value of the user authentication token
     */
    insertedToken(value: string): void {
        const token: JwTokenClass = new JwTokenClass();

        token.decode(value);
        sessionStorage.setItem('token', JSON.stringify(token));
        this.connectedObservable.next(true);
    }

    /**
     * @description This method removes the user's authentication token
     */
    removedToken(): void {
        sessionStorage.removeItem('token');
        this.connectedObservable.next(false);

        sessionStorage.removeItem('user');
        this.userObservable.next(null);
    }

    /**
     * @description This method allows to know if the authentication token is still valid
     */
    expiredToken(): boolean {
        const value: string = sessionStorage.getItem('token');

        if (value) {
            const token: JwTokenClass = JSON.parse(value);
            const expired: boolean = token.expiration < (Date.now() / 1000);

            if (expired) {
                this.removedToken();
            }
            this.connectedObservable.next(!expired);
            this.userObservable.next(JSON.parse(sessionStorage.getItem('user')));

            return expired;
        }
        this.connectedObservable.next(false);
        return true;
    }

    /**
     * @description The method allows you to retrieve the listener to know if the user is connected
     */
    get connectedObservable(): CustomSubjectClass<boolean> {
        return this._connectedObservable;
    }

    /**
     * @description The method is used to assign the listener to know if the user is connected
     *
     * @param value Value of the new listener to know if the user is connected
     */
    set connectedObservable(value: CustomSubjectClass<boolean>) {
        this._connectedObservable = value;
    }

    /**
     * @description The method allows you to retrieve the listener and sender to retrieve user model data (when a user is connected)
     */
    get userObservable(): CustomSubjectClass<UserClass> {
        return this._userObservable;
    }

    /**
     * @description The method is used to assign the listener and sender to retrieve user model data (when a user is connected)
     *
     * @param value Value of the new listener and sender to retrieve user model data (when a user is connected)
     */
    set userObservable(value: CustomSubjectClass<UserClass>) {
        this._userObservable = value;
    }
}
