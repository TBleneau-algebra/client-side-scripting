import {Subject} from 'rxjs';

/**
 * @description This class is an extension to Subject class from RxJS.
 * This extension keeps the last value passed through next() call.
 *
 * Note : You should NEVER change the value of _last without using next(). That's the reason why it don't have a setter.
 */
export class CustomSubjectClass<T> extends Subject<T> {

    /**
     * @description Last value passed through next() call
     */
    private _last: T = null;

    /**
     * @description Constructor of CustomSubjectClass class
     *
     * The constructor creates an instance of the CustomSubjectClass class and specifies the default values
     * the input and output variables of the class.
     */
    constructor() {
        super();
    }

    /**
     * @description The subject next method is used to send messages to an observable which are then sent to all angular components
     * that are subscribers of that observable
     *
     * @param value Value to send
     */
    next(value?: T): void {
        this._last = value;
        super.next(value);
    }

    /**
     * @description The method allows you to retrieve the last value passed through next() call
     */
    get last(): T {
        return this._last;
    }
}
