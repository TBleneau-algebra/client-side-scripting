import * as moment from 'moment';
import {CreditCardClass} from './credit-card.class';

export class BillInformationClass {
    Id?: string;
    Date: string;
    BillNumber: string;
    CustomerId: string;
    SellerId: string;
    CreditCardId: string;
    CreditCard?: CreditCardClass;
    Comment: string;
    Seller?: any;

    constructor() {
        this.formatDate(new Date());
        this.Comment = 'No comment';
    }

    formatDate(date: Date): void {
        this.Date = moment(date).format('YYYY-MM-DD');
    }
}
