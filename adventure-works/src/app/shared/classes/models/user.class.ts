/**
 * @description This class implements what send to the API to update the customer or what the API respond
 */
export class UserClass {
    id?: number;
    username: string;
    password: string;
    name: string;
    img?: string;
    registerTime?: string;

    constructor() {
        this.img = '/assets/img/user.png';
    }
}
