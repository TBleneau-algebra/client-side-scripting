export class CreditCardClass {
    Type: string;
    CardNumber: string;
    ExpirationMonth: string;
    ExpirationYear: string;

    constructor() {
        this.Type = null;
        this.CardNumber = null;
    }

    divide(value: string): void {
        if (value) {
            const date: Array<string> = value.split('/');

            if (value && value.length > 0) {
                this.ExpirationMonth = date[0];
                this.ExpirationYear = date[1];
            }
        }
    }
}
