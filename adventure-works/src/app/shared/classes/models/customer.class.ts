/**
 * @description This class implements what send to the API to update the customer or what the API respond
 */
export class CustomerClass {
    Id?: number;
    Name: string;
    Surname: string;
    Email: string;
    Telephone: string;
    CityId: string;
}
