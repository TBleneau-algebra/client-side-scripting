import {BillInformationClass} from './bill-information.class';
import {CreditCardClass} from './credit-card.class';
import {ItemClass} from './item.class';
import {CustomerClass} from './customer.class';

export class BillClass {
    BillInformation: BillInformationClass;
    CreditCard?: CreditCardClass;
    Items: Array<ItemClass>;
    Customer?: CustomerClass;

    constructor() {
        this.BillInformation = new BillInformationClass();
        this.CreditCard = new CreditCardClass();
        this.Items = new Array<ItemClass>();
    }

}
