import * as jwt_decode from 'jwt-decode';

/**
 * @description This class implements the JW Token used to request the API
 */
export class JwTokenClass {

    /**
     * @description
     */
    public username: string;

    /**
     * @description
     */
    public expiration: number;

    /**
     * @description
     */
    public issuedAt: number;

    /**
     * @description
     */
    public notBefore: number;

    /**
     * @description
     */
    public value: string;

    /**
     *
     */
    constructor() {
    }

    /**
     * @description
     *
     * @param token
     */
    public decode(token: string): void {
        const decoded: any = jwt_decode(token);

        this.value = token;
        this.username = decoded.unique_name;
        this.expiration = decoded.exp;
        this.issuedAt = decoded.iat;
        this.notBefore = decoded.nbf;
    }
}
