/**
 * Import of Angular's module
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/**
 * Import of component
 */
import {SpinnerComponent} from './spinner.component';

/**
 * Spinner module of the Application. This module retrieves spinner component
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SpinnerComponent
  ],
  exports: [
    SpinnerComponent
  ],
})
export class SpinnerModule {
}
