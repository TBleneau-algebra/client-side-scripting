/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {ModalComponent} from './modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    imports: [
        CommonModule,
        NgbModule
    ],
    exports: [
        ModalComponent
    ],
    declarations: [
        ModalComponent
    ]
})
export class ModalModule {
}
