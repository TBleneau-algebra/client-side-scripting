import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-modal-user',
    template: ''
})
export class ModalComponent implements OnInit, OnDestroy {

    /**
     * @description A reference to the newly opened modal returned by the NgbModal.open() method.
     */
    private dialog: NgbModalRef;

    /**
     * @description Sender to deactivate the modal
     *
     * A Subject is a special type of Observable that allows values to be multicasted to many Observers.
     * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
     *
     * Subject are multicast.
     */
    private destroy: Subject<any>;

    /**
     * @description Constructor of the component ModalComponent
     *
     * The constructor creates an instance of the component ModalComponent and specifies the default values
     * of the component's input and output variables.
     */
    constructor(private modalService: NgbModal, private activatedRoute: ActivatedRoute, private router: Router) {
        this.dialog = null;
        this.destroy = new Subject<any>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this.destroy)).subscribe(() => {
            this.dialog = this.modalService.open(this.activatedRoute.snapshot.data.component,
                {centered: true, windowClass: 'modal-custom'});

            this.dialog.componentInstance.kill.asObservable().subscribe(() => {
                this.dialog.close();
                this.destroy.next();
            });

            this.dialog.result.then(result => {
                this.router.navigate(['/']).then();
            }, reason => {
                this.router.navigate(['/']).then();
            });
        });
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        this.destroy.next();
    }

}
