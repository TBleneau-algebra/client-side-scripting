import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {CreditCardClass} from '../../../classes/models/credit-card.class';
import * as moment from 'moment';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-form-credit-card',
    templateUrl: './form-credit-card.component.html',
})
export class FormCreditCardComponent extends FormComponent implements OnInit, OnDestroy {

    /**
     * @description This variable is used to enable or disable the form button depending on a parameter external to the form
     */
    @Input() public valid: boolean;

    /**
     * @description Values of available credit card types
     */
    public cards: Array<string> = ['Visa', 'MasterCard', 'Discover', 'American Express'];

    /**
     * @description Constructor of FormCreditCardComponent component
     *
     * The constructor creates an instance of the FormCreditCardComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     */
    constructor(private formBuilder: FormBuilder) {
        super();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initModel();

        this.receiver.subscribe(value => {
            (value) ? this.initModel() : undefined;
        });

        this.formGroup = this.formBuilder.group(
            {
                card: [null, Validators.compose([
                    Validators.required,
                ])],
                cardNumber: [null, Validators.compose([
                    Validators.required,
                ])],
                cardExpiration: [null, Validators.compose([
                    Validators.required,
                ])],
            },
            {
                validators: [
                    FormCreditCardComponent.dateMatchValidator
                ]
            }
        );
    }

    /**
     * @description This method is used to initialize the data model to be sent to the API when creating a customer
     */
    initModel(): void {
        if (this.model) {
            (this.formGroup) ? this.formGroup.controls['cardExpiration'].setValue(null) : undefined;
            delete this.model;
        }
        this.model = new CreditCardClass();
    }

    /**
     * @description This method allows you to format the date in the format'MM/YYY'.
     * It refuses any characters other than numbers and '/'.
     * It respects the pattern {2}/{4}.
     *
     * @param event Input value
     */
    replaceExpirationDate(event: any): void {
        event.target.value = event.target.value
            .replace(/^(\d\d)(\d)$/g, '$1/$2')
            .replace(/[^\d\/]/g, '');
    }

    /**
     * @description This method allows you to refuse any characters other than numbers
     *
     * @param event Input value
     * @param defaultValue Default value to assign when the value of the input is null or empty
     */
    replaceNumber(event: any, defaultValue?: number): void {
        event.target.value = event.target.value
            .replace(/[^\d]/g, '');

        if (defaultValue && (!event.target.value || event.target.value === '')) {
            event.target.value = defaultValue;
        }
    }

    /**
     * @description This method is used to validate whether the format of a date is valid
     *
     * @param control This is the base class for FormControl, FormGroup, and FormArray
     */
    private static dateMatchValidator(control: AbstractControl) {
        const date = control.get('cardExpiration').value;
        const check = moment(date, 'MM/YYYY');

        if (date === null || !moment(date, 'MM/YYYY').isValid() || !check.isSameOrAfter(moment(new Date()))) {
            control.get('cardExpiration').setErrors({'dateError': true});
        }
    }
}
