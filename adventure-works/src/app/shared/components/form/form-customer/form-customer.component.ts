import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {CustomerClass} from '../../../classes/models/customer.class';
import {CacheService} from '../../../services/cache/cache.service';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-form-customer',
    templateUrl: './form-customer.component.html',
})
export class FormCustomerComponent extends FormComponent implements OnInit, OnChanges {

    /**
     * @description Title of the form
     */
    @Input() public title: string;

    /**
     * @description Option to make the form editable and thus be able to delete it
     */
    @Input() public editable: boolean;

    /**
     * @description Cities retrieved via the API
     */
    private _cities: Array<any>;

    /**
     * @description Constructor of FormCustomerComponent component
     *
     * The constructor creates an instance of the FormCustomerComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param cacheService A reference to the cache service of the application
     */
    constructor(private formBuilder: FormBuilder, private cacheService: CacheService) {
        super();
        this.cities = [];
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initModel();

        this.cities = this.cacheService.load('cities');

        this.receiver.subscribe(value => {
            (value) ? this.initModel() : undefined;
        });

        this.formGroup = this.formBuilder.group(
            {
                name: [null, Validators.compose([
                    Validators.required,
                ])],
                surname: [null, Validators.compose([
                    Validators.required,
                ])],
                email: [null, Validators.compose([
                    Validators.required,
                    Validators.email
                ])],
                telephone: [null, Validators.compose([
                    Validators.required,
                ])],
                city: [null, Validators.compose([
                    Validators.required,
                ])]
            }
        );
    }

    /**
     * @description A lifecycle hook that is called when any data-bound property of a directive changes.
     * Define an ngOnChanges() method to handle the changes
     *
     * @param changes A hashtable of changes represented by SimpleChange objects stored at the declared property name they belong
     * to on a Directive or Component. This is the type passed to the ngOnChanges hook
     */
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('model') && this.formGroup) {
            this.formGroup.controls['city'].setValue(changes.model.currentValue.CityId);
        }
    }

    /**
     * @description This method is used to initialize the data model to be sent to the API when creating a customer
     */
    initModel(): void {
        if (this.model) {
            delete this.model;
        }
        this.model = new CustomerClass();
    }

    /**
     * @description This method allows you to format the phone number in the format'xxx-xxx-xxxx'.
     * It refuses any characters other than numbers and '-'.
     * It respects the pattern {3}-{3}-{4}.
     *
     * @param event Input value
     */
    replaceTelephoneValue(event: any): void {
        event.target.value = event.target.value
            .replace(/^(\d\d\d)(\d)$/g, '$1-$2')
            .replace(/^(\d\d\d\-\d\d\d)(\d+)$/g, '$1-$2')
            .replace(/[^\d\-]/g, '');
    }

    /**
     * @description The method allows you to retrieve the cities retrieved via the API
     */
    get cities(): Array<any> {
        return this._cities;
    }

    /**
     * @description The method allows you to assign the cities retrieved via the API
     *
     * @param value Value of the new cities retrieved via the API
     */
    set cities(value: Array<any>) {
        this._cities = value;
    }
}
