import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-form',
    template: '',
})
export class FormComponent implements OnDestroy {

    /**
     * @description Data model to send to the API
     */
    @Input() public model: any;

    /**
     * @description Observables provide support for passing messages between publishers and subscribers in your application
     *
     * This Observable allows to know if the action carried out by the user has been carried out correctly
     */
    @Input() public receiver: Observable<boolean>;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the FormComponent, this EventEmitter allows the sending of the action performed by the user and the data relating to this action
     */
    @Output() public emitter: EventEmitter<{ action: string, model: any }>;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description Constructor of FormComponent component
     *
     * The constructor creates an instance of the FormComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor() {
        this.receiver = new Observable<boolean>();
        this.emitter = new EventEmitter<{ action: string, model: any }>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        delete this.model;
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description This method checks if a HTML element is focused
     *
     * @param value HTML element to check
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }


    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }
}
