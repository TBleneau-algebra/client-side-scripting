/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of application modules
 */
import {ThemeModule} from '../../theme/theme.module';
/**
 * Import of application components
 */
import {FormCustomerComponent} from './form-customer/form-customer.component';
import {FormCreditCardComponent} from './form-credit-card/form-credit-card.component';
import {FormComponent} from './form.component';
import {FormUserComponent} from './form-user/form-user.component';


@NgModule({
    imports: [
        CommonModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ],
    declarations: [
        FormComponent,
        FormCustomerComponent,
        FormCreditCardComponent,
        FormUserComponent
    ],
    exports: [
        FormComponent,
        FormCustomerComponent,
        FormCreditCardComponent,
        FormUserComponent
    ]
})
export class FormModule {
}
