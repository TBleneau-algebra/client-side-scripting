import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {FormComponent} from '../form.component';
import {UserClass} from '../../../classes/models/user.class';
import {ValidatorClass} from '../../../classes/validator/validator.class';

@Component({
    selector: 'app-form-user',
    templateUrl: './form-user.component.html',
})
export class FormUserComponent extends FormComponent implements OnInit {

    /**
     * @description Constructor of FormUserComponent component
     *
     * The constructor creates an instance of the FormUserComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     */
    constructor(private formBuilder: FormBuilder) {
        super();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initModel();

        this.receiver.subscribe(value => {
            (value) ? this.initModel() : undefined;
        });

        this.formGroup = this.formBuilder.group(
            {
                name: [null, Validators.compose([
                    Validators.required,
                ])],
                username: [null, Validators.compose([
                    Validators.required,
                ])],
                password: [null, Validators.compose([
                    Validators.required,
                    ValidatorClass.patternValidator(/\d/, {hasNumber: true}),
                    ValidatorClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
                    ValidatorClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
                    ValidatorClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
                    Validators.minLength(8),
                    Validators.maxLength(64)
                ])]
            }
        );
    }

    /**
     * @description This method is used to initialize the data model to be sent to the API when creating a customer
     */
    initModel(): void {
        if (this.model) {
            delete this.model;
        }
        this.model = new UserClass();
    }
}
