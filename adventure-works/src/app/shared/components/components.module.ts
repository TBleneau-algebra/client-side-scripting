/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's modules
 */
import {HeaderModule} from './header/header.module';
import {ColumnModule} from './column/column.module';
import {LoadableModule} from './loadable/loadable.module';
import {TableModule} from './table/table.module';
import {FormModule} from './form/form.module';
import {ModalModule} from './modal/modal.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ColumnModule,
        HeaderModule,
        LoadableModule,
        TableModule,
        FormModule,
        ModalModule
    ],
    exports: [
        ColumnModule,
        HeaderModule,
        LoadableModule,
        TableModule,
        FormModule,
        ModalModule
    ]
})

export class ComponentsModule {
}
