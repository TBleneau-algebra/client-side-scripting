import {Component, Input, NgZone, OnInit} from '@angular/core';
import {NbSidebarService} from '@nebular/theme';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert/alert.service';
import {UserRequestService} from '../../services/request/user-request.service';
import {UserClass} from '../../classes/models/user.class';

@Component({
    selector: 'app-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

    /**
     * @description User connected to the application
     */
    private _user: UserClass;

    /**
     * @description Application title
     */
    @Input() public title: string;

    /**
     * @description User login status
     */
    public connected: boolean;

        /**
         * @description Constructor of HeaderComponent component
         *
         * The constructor creates an instance of the HeaderComponent component and specifies the default values
         * the input and output variables of the component.
         *
         * @param sidebarService A reference to the Nebular sidebar service
         * @param alertService A reference to the alert service of the application
         * @param router Angular Router enables navigation from one view to the next as users perform application tasks
         * @param zone An injectable service for executing work inside or outside of the Angular zone
         * @param userRequestService A reference to the http request service used for user information in the application
         */
    constructor(private sidebarService: NbSidebarService, private alertService: AlertService, private router: Router,
                private zone: NgZone, private userRequestService: UserRequestService) {
        this.user = null;
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit() {
        this.userRequestService.connectedObservable.asObservable().subscribe(value => {
            this.connected = value;
        });
        this.userRequestService.userObservable.asObservable().subscribe(value => {
            this.user = value;
        });
        this.userRequestService.expiredToken();
    }

    /**
     * @description This method logs out the user via the UserRequestService service
     */
    logout(): void {
        this.alert().then(response => {
            if (response) {
                this.userRequestService.removedToken();
            }
        });
    }

    /**
     * @description The method uses the Angular router and allows you to navigate the application's routes
     *
     * @param path Path to navigate to
     * @param username Username of the user to pass in url parameter
     */
    navigate(path: string, username?: string): void {
        this.router.navigate([path], {queryParams: {username: username}}).then(() => {
        });
    }

    /**
     * @description The method allows you to reduce or enlarge the navigation bar
     */
    toggle(): void {
        this.sidebarService.toggle(true, 'menu-sidebar');
    }

    /**
     * @description This method ensures that the user is willing to disconnect from the platform.
     * It uses the AlertService service
     */
    private alert(): PromiseLike<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.alertService.confirm('Are you sure you want to log out?',
                'Yes', 'No')
                .subscribe((confirm) => {
                    resolve(confirm);
                });
        });
    }

    /**
     * @description The method allows you to retrieve the user connected to the application
     */
    get user(): UserClass {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user connected to the application
     *
     * @param value Value of the new user connected to the application
     */
    set user(value: UserClass) {
        this._user = value;
    }
}
