import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
    selector: 'app-table',
    template: '',
})
export class TableComponent implements OnChanges {

    /**
     * @description Data to display and retrieved via the API
     */
    @Input() public data: Array<any>;

    /**
     * @description Headers to display in the table
     */
    @Input() public headers: Array<string>;

    /**
     * @description Define the starting point for displaying data in table
     */
    @Input() public pageFrom: number;

    /**
     * @description Define the size of a page to be displayed in the table
     */
    @Input() public pageSize: number;

    /**
     * @description Define the total number of pages
     */
    public pageTotal: number;

    /**
     * @description Define the actual page number
     */
    public pageActual: number;

    /**
     * @description Constructor of TableComponent component
     *
     * The constructor creates an instance of the TableComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor() {
        this.data = [];
        this.headers = [];
        this.pageFrom = 0;
        this.pageSize = 10;
    }

    /**
     * @description A lifecycle hook that is called when any data-bound property of a directive changes.
     * Define an ngOnChanges() method to handle the changes
     *
     * @param changes A hashtable of changes represented by SimpleChange objects stored at the declared property name they belong
     * to on a Directive or Component. This is the type passed to the ngOnChanges hook
     */
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hasOwnProperty('data') && changes.data.currentValue) {
            this.pageActual = 1;
            this.pageTotal = Math.ceil(changes.data.currentValue.length / this.pageSize);
        }
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description This method allows you to navigate through the data table and more specifically on the next page.
     *
     * @param index Optional index on which to navigate
     */
    next(index?: number): void {
        if (index && this.pageActual < this.pageTotal) {
            this.pageFrom = this.pageSize * (index - 1);
            this.pageActual = index;
        } else if (this.pageActual < this.pageTotal) {
            this.pageFrom += this.pageSize;
            this.pageActual += 1;
        }
    }

    /**
     * @description This method allows you to navigate through the data table and more specifically on the previous page
     */
    prev(): void {
        if (this.pageFrom !== 0) {
            this.pageFrom -= this.pageSize;
            this.pageActual -= 1;
        }
    }
}
