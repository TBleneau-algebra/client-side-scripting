import {Component} from '@angular/core';
import {TableComponent} from '../table.component';
import {Router} from '@angular/router';
import {BillClass} from '../../../classes/models/bill.class';

@Component({
    selector: 'app-table-bill',
    templateUrl: './table-bill.component.html',
})
export class TableBillComponent extends TableComponent {

    /**
     * @description Constructor of TableBillComponent component
     *
     * The constructor creates an instance of the TableBillComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private router: Router) {
        super();
    }

    /**
     * @description This method allows you to navigate to the information of the selected customer.
     * The customer's ID is used to load the bill's information.
     *
     * @param item bill used to load the bill's information
     */
    navigate(item: BillClass): void {
            this.router.navigate(['/pages/bill/' + item.BillInformation.Id], {state: {bill: item}}).then(() => {
            });
    }

    /**
     * @description This method calculates the total price on the invoice by adding the total price of each item
     *
     * @param items Items on the bill
     */
    totalPrice(items: Array<any>) {
        let total: number = 0;

        if (items) {
            items.forEach(item => {
                total += item.TotalPrice;
            });
        }
        return Math.round(total * 100) / 100;
    }
}
