/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application modules
 */
import {ThemeModule} from '../../theme/theme.module';
/**
 * Import of application components
 */
import {TableComponent} from './table.component';
import {TableCustomerComponent} from './table-customer/table-customer.component';
import {TableBillComponent} from './table-bill/table-bill.component';
import {TableProductComponent} from './table-product/table-product.component';

@NgModule({
    imports: [
        CommonModule,
        ThemeModule
    ],
    declarations: [
        TableComponent,
        TableCustomerComponent,
        TableBillComponent,
        TableProductComponent
    ],
    exports: [
        TableComponent,
        TableCustomerComponent,
        TableBillComponent,
        TableProductComponent
    ]
})
export class TableModule {
}
