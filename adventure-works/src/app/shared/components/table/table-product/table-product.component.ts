import {Component} from '@angular/core';
import {TableComponent} from '../table.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-table-product',
    templateUrl: './table-product.component.html',
})
export class TableProductComponent extends TableComponent {

    /**
     * @description Constructor of TableProductComponent component
     *
     * The constructor creates an instance of the TableProductComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private router: Router) {
        super();
    }
}
