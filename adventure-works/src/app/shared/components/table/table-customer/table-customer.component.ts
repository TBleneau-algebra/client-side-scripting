import {Component} from '@angular/core';
import {TableComponent} from '../table.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-table-customer',
    templateUrl: './table-customer.component.html',
})
export class TableCustomerComponent extends TableComponent {

    /**
     * @description Constructor of TableCustomerComponent component
     *
     * The constructor creates an instance of the TableCustomerComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private router: Router) {
        super();
    }

    /**
     * @description This method allows you to navigate to the information of the selected customer.
     * The customer's ID is used to load the customer's information.
     *
     * @param id customer's ID used to load the customer's information.
     */
    navigate(id: number): void {
        this.router.navigate(['/pages/customer/' + id]).then(() => {
        });
    }
}
