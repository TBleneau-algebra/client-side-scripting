/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {CustomerEntityComponent} from './customer-entity.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
import {ComponentsModule} from '../../../shared/components/components.module';


@NgModule({
    imports: [
        CommonModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        CustomerEntityComponent,
    ],
    exports: [
        CustomerEntityComponent
    ]
})
export class CustomerEntityModule {
}
