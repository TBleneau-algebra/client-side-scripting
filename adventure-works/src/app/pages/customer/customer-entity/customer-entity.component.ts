import {Component, OnDestroy, OnInit} from '@angular/core';
import {CustomerClass} from '../../../shared/classes/models/customer.class';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../../../shared/services/request/request.service';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {
    GET_BILL_ITEM_BY_BILL_ID,
    GET_CUSTOMER_BILL_BY_CUSTOMER_ID,
    GET_CUSTOMER_BY_ID,
    POST_DELETE_CUSTOMER,
    POST_EDIT_CUSTOMER
} from '../../../shared/data/api-path.data';
import {AlertService} from '../../../shared/services/alert/alert.service';

@Component({
    selector: 'app-customer-entity',
    templateUrl: './customer-entity.component.html',
})
export class CustomerEntityComponent implements OnInit, OnDestroy {

    /**
     * @description Information of the selected customer retrieved via the API
     */
    private _customer: CustomerClass;

    /**
     * @description List of invoices of the selected user retrieved via the API
     */
    private _bills: Array<any>;

    /**
     * @description Constructor of CustomerEntityComponent component
     *
     * The constructor creates an instance of the CustomerEntityComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private activatedRoute: ActivatedRoute, private requestService: RequestService, private alertService: AlertService,
                private router: Router) {
        this.bills = [];
        this.customer = new CustomerClass();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        const id: string = this.activatedRoute.snapshot.paramMap.get('id');

        this.loadCustomer(id);
        this.loadBill(id);
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load a customer thanks to his identifier
     */
    loadCustomer(id: string): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.get(GET_CUSTOMER_BY_ID + id)
            .then(response => {
                this.customer = response.responseData;
            })
            .catch(error => {
                (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message)
                    : this.alertService.error('An error occurred during retrieving customer data');
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the selected customer bills available
     *
     * @param id Selected customer id
     */
    loadBill(id: string): void {
        this.bills = [];

        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.requestService.get(GET_CUSTOMER_BILL_BY_CUSTOMER_ID + id)
            .then(response => {
                response.responseData.forEach(item => {
                    this.requestService.get(GET_BILL_ITEM_BY_BILL_ID + item.Id)
                        .then(response => {
                            this.bills = [...this.bills, {
                                BillInformation: item,
                                Customer: this.customer,
                                Items: response.responseData
                            }];
                        })
                        .catch(() => {
                            return [];
                        });
                });
            })
            .catch(() => {
                this.alertService.error('An error occurred while loading bills');
            });
    }

    /**
     * @description This method is called when the user information output form sends an event
     *
     * @param event Event containing the action to be performed and the data to be processed
     */
    receive(event: any): void {
        switch (event.action) {
            case 'edit':
                return this.editCustomer(event);
            case 'delete':
                return this.deleteCustomer(event);
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and edit a customer
     */
    editCustomer(event: any): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.post(event.model, POST_EDIT_CUSTOMER)
            .then(response => {
                this.customer = response.responseData;
                this.alertService.success('The customer has been successfully edited!');
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to edit a customer, you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error has occurred. Try again later.');
                }
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and delete a customer
     */
    deleteCustomer(event: any): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.post(event.model, POST_DELETE_CUSTOMER)
            .then(() => {
                this.router.navigate(['/pages/customer']).then(() => {
                    this.alertService.success('The customer has been successfully deleted!');
                });
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to delete a customer, you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error has occurred. Try again later.');
                }
            });
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description The method allows you to retrieve the information of the selected customer retrieved via the API
     */
    get customer(): CustomerClass {
        return this._customer;
    }

    /**
     * @description The method is used to assign the information of the selected customer retrieved via the API
     *
     * @param value Value of the new information of the selected customer retrieved via the API
     */
    set customer(value: CustomerClass) {
        this._customer = value;
    }

    /**
     * @description The method allows you to retrieve the list of invoices of the selected user retrieved via the API
     */
    get bills(): Array<any> {
        return this._bills;
    }

    /**
     * @description The method is used to assign the list of invoices of the selected user retrieved via the API
     *
     * @param value Value of the new list of invoices of the selected user retrieved via the API
     */
    set bills(value: Array<any>) {
        this._bills = value;
    }
}
