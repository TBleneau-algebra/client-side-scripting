/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {CustomerComponent} from './customer.component';
/**
 * Import of application's modules
 */
import {CustomerListModule} from './customer-list/customer-list.module';
import {CustomerRoutingModule} from './customer-routing.module';
import {CustomerEntityModule} from './customer-entity/customer-entity.module';


@NgModule({
    imports: [
        CommonModule,
        CustomerRoutingModule,
        CustomerListModule,
        CustomerEntityModule
    ],
    declarations: [
        CustomerComponent,
    ]
})
export class CustomerModule {
}
