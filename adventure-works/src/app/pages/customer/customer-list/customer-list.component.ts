import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {RequestService} from '../../../shared/services/request/request.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {CacheService} from '../../../shared/services/cache/cache.service';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {GET_CUSTOMERS, POST_ADD_CUSTOMER} from '../../../shared/data/api-path.data';
import {Router} from '@angular/router';

@Component({
    selector: 'app-customer-list',
    templateUrl: './customer-list.component.html',
})
export class CustomerListComponent implements OnInit, OnDestroy {

    /**
     * @description Customers information retrieved via the API
     */
    private _customers: Array<any>;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the CustomerListComponent, this EventEmitter allows the sending of a boolean to indicate that the http request to create
     * a user has been performed
     */
    private _emitter: EventEmitter<boolean>;

    /**
     * @description Constructor of CustomerListComponent component
     *
     * The constructor creates an instance of the CustomerListComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param cacheService A reference to the cache service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private requestService: RequestService, private alertService: AlertService, private cacheService: CacheService,
                private router: Router) {
        this.customers = [];
        this.emitter = new EventEmitter<boolean>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadCustomer();

    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the customers available
     */
    loadCustomer(): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.get(GET_CUSTOMERS)
            .then(response => {
                this.customers = this.assignCity(response.responseData);
            })
            .catch(() => {
                this.alertService.error('An error occurred while loading customers');
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and create a new customer
     */
    createCustomer(event: any): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(2));

        this.requestService.post(event.model, POST_ADD_CUSTOMER)
            .then(() => {
                this.alertService.success('The customer has been successfully created!');
                this.emitter.emit(true);
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to create a customer, you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error has occurred. Try again later');
                }
            });
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description This method allows you to navigate to the information of the selected customer.
     * The customer's ID is used to load the customer's information.
     *
     * @param id customer's ID used to load the customer's information.
     */
    navigate(id: number): void {
        this.router.navigate(['/pages/customer/' + id]).then(() => {
        });
    }

    /**
     * @description This method allows to assign the City object to a user according to the CityId property
     *
     * @param data Users retrieved via API
     */
    private assignCity(data: Array<any>): any {
        const cities: Array<any> = this.cacheService.load('cities');

        data.forEach(item => {
            if (cities) {
                item['City'] = cities.find(city => city.Id === item.CityId);
            } else {
                item ['City'] = 'Undefined';
            }
        });
        return this.assignState(data);
    }

    /**
     * @description This method allows to assign the State object to a user according to the City.StateId property
     *
     * @param data Users retrieved via API
     */
    private assignState(data: Array<any>): any {
        const states: Array<any> = this.cacheService.load('states');

        data.forEach(item => {
            if (states) {
                if (item.hasOwnProperty('City')) {
                    item['State'] = states.find(state => state.Id === item.City.StateId);
                }
            } else {
                item['State'] = 'Undefined';
            }
        });
        return data;
    }

    /**
     * @description The method allows you to retrieve the customers information retrieved via the API
     */
    get customers(): Array<any> {
        return this._customers;
    }

    /**
     * @description The method is used to assign the customers information retrieved via the API
     *
     * @param value Value of the new customers information retrieved via the API
     */
    set customers(value: Array<any>) {
        this._customers = value;
    }

    /**
     * @description The method allows you to retrieve the EventEmitter which allows the sending of a boolean to indicate that
     * the http request to create a user has been performed
     */
    get emitter(): EventEmitter<boolean> {
        return this._emitter;
    }

    /**
     * @description The method is used to assign the EventEmitter which allows the sending of a boolean to indicate that
     * the http request to create a user has been performed
     *
     * @param value Value of the new EventEmitter
     */
    set emitter(value: EventEmitter<boolean>) {
        this._emitter = value;
    }
}
