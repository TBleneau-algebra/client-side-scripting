/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of application's components
 */
import {CustomerListComponent} from './customer-list.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
import {ComponentsModule} from '../../../shared/components/components.module';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        CustomerListComponent,
    ],
    exports: [
        CustomerListComponent
    ]
})
export class CustomerListModule {
}
