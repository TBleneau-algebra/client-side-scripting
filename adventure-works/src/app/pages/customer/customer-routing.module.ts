/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerComponent} from './customer.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {CustomerEntityComponent} from './customer-entity/customer-entity.component';
/**
 * Import of application's services
 */
import {PagesGuardService} from '../../shared/services/guards/pages-guard.service';

const routes: Routes = [{
    path: '',
    component: CustomerComponent,
    children: [
        {
            path: '',
            component: CustomerListComponent
        },
        {
            path: ':id',
            canActivate: [PagesGuardService],
            component: CustomerEntityComponent
        },
        {
            path: '**',
            component: NotFoundComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CustomerRoutingModule {
}
