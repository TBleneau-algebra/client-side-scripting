import {AfterViewInit, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';
import {menuData, menuMobileData} from '../shared/data/menu.data';
import {RequestService} from '../shared/services/request/request.service';
import {GET_CATEGORIES, GET_CITIES, GET_SELLERS, GET_STATES, GET_SUBCATEGORIES_BY_CATEGORY_ID} from '../shared/data/api-path.data';
import {CacheService} from '../shared/services/cache/cache.service';
import {UserRequestService} from '../shared/services/request/user-request.service';
import {LoaderSemaphoreClass} from '../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {UserClass} from '../shared/classes/models/user.class';

@Component({
    selector: 'app-pages',
    template: `
        <app-one-column>
            <app-header [title]="'Adventure Works Application'"></app-header>
            <nb-menu [items]="menuItems" [autoCollapse]="false">
            </nb-menu>
            <router-outlet></router-outlet>
        </app-one-column>`
})
export class PagesComponent implements OnInit, OnDestroy, AfterViewInit {

    /**
     * @description Items in the component menu NbMenuComponent
     *
     * A map is used to ensure the uniqueness of the items by title.
     * The elements are sorted in {@link menuItems}.
     */
    private _menuItems: Map<string, NbMenuItem> = new Map<string, NbMenuItem>();

    /**
     * @description Constructor of PagesComponent component
     *
     * The constructor creates an instance of the PagesComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param cacheService A reference to the cache service of the application
     * @param userRequestService A reference to the http request service used for user information in the application
     */
    constructor(private requestService: RequestService, private cacheService: CacheService,
                private userRequestService: UserRequestService) {
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        if (window.innerWidth >= 768) {
            menuData.forEach(item => this._menuItems.set(item.title, item));
        } else {
            this.assignMobileItems();
        }

        this.loadCities();
        this.loadStates();
        this.loadSellers();
        this.loadCategories();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular has finished to initialize the view of a component.
     * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
     */
    ngAfterViewInit(): void {
        if (this.userRequestService.expiredToken()) {
            this.userRequestService.removedToken();
        }
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description Decorator that declares a DOM event to listen for, and provides a handler method to run when that event occurs.
     * This DOM event listen to 'resize' event
     *
     * @param event DOM event
     */
    @HostListener('window:resize', ['$event'])
    onResize(event: any): void {
        if (window.innerWidth < 768) {
            this.assignMobileItems();
        } else {
            this.menuItems = menuData;
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the cities available
     */
    loadCities(): void {
        if (this.cacheService.load('cities') === null) {
            this.requestService.activeLoader(new LoaderSemaphoreClass(1));

            this.requestService.get(GET_CITIES)
                .then(response => {
                    this.cacheService.save('cities', response.responseData);
                })
                .catch(() => {
                });
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the states available
     */
    loadStates(): void {
        if (this.cacheService.load('states') === null) {
            this.requestService.activeLoader(new LoaderSemaphoreClass(2));

            this.requestService.get(GET_STATES)
                .then(response => {
                    this.cacheService.save('states', response.responseData);
                })
                .catch(error => {
                });
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the categories available
     */
    loadCategories(): void {
        if (this.cacheService.load('categories') === null) {
            this.requestService.activeLoader(new LoaderSemaphoreClass(3));

            this.requestService.get(GET_CATEGORIES)
                .then(response => {
                    this.cacheService.save('categories', response.responseData);
                    this.loadSubCategories(response.responseData);
                })
                .catch(() => {
                });
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the categories available
     */
    loadSellers(): void {
        if (this.cacheService.load('sellers') === null) {
            this.requestService.activeLoader(new LoaderSemaphoreClass(4));

            this.requestService.get(GET_SELLERS)
                .then(response => {
                    this.cacheService.save('sellers', response.responseData);
                })
                .catch(() => {
                });
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the categories available
     */
    loadSubCategories(categories: Array<any>): void {
        if (this.cacheService.load('subcategories') === null) {
            let subcategories: Array<any> = [];

            categories.forEach((item, index) => {
                this.requestService.activeLoader(new LoaderSemaphoreClass(5 + index));

                this.requestService.get(GET_SUBCATEGORIES_BY_CATEGORY_ID + item.Id)
                    .then(response => {
                        subcategories = [...subcategories, ...response.responseData];
                        this.cacheService.save('subcategories', subcategories);
                    })
                    .catch(() => {
                    });
            });
        }
    }

    /**
     * @description This method makes allows you to assign the menu items present in the header, in the sidebar
     */
    private assignMobileItems(): void {
        let items: Array<NbMenuItem> = [...menuMobileData];
        const user: UserClass = this.cacheService.load('user');

        if (user) {
            items.find(item => item.title === 'Profile').queryParams = {username: user.username};
            items = items.filter(item => item.title !== 'Login');
            this.menuItems = items;
        } else {
            items = items.filter(item => (item.title !== 'Logout' && item.title !== 'Profile'));
            this.menuItems = items;
        }
    }

    /**
     * @description The method allows you to retrieve the sorted items from the NbMenuComponent menu.
     */
    get menuItems(): Array<NbMenuItem> {
        return Array.from(this._menuItems.values());
    }

    /**
     * @description The method is used to assign the menu items of the NbMenuComponent
     * Empty the item map and fill it with the input table using the item title as the key.
     *
     * @param value Value of the new NbMenuComponent menu items
     */
    set menuItems(value: Array<NbMenuItem>) {
        this._menuItems.clear();
        value.forEach(item => this._menuItems.set(item.title, item));
    }
}
