import {Component, OnDestroy, OnInit} from '@angular/core';
import {RequestService} from '../../shared/services/request/request.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {CacheService} from '../../shared/services/cache/cache.service';
import {LoaderSemaphoreClass} from '../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {GET_PRODUCTS_BY_SUBCATEGORY_ID} from '../../shared/data/api-path.data';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
})
export class ProductComponent implements OnInit, OnDestroy {

    /**
     * @description List of available products retrieved via the API
     */
    private _products: Array<any>;

    /**
     * @description Constructor of ProductComponent component
     *
     * The constructor creates an instance of the ProductComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param cacheService A reference to the cache service of the application
     */
    constructor(private requestService: RequestService, private alertService: AlertService, private cacheService: CacheService) {
        this.products = [];
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadProducts();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the selected customer bills available
     */
    loadProducts(): void {
        const subcategories: Array<any> = this.cacheService.load('subcategories');
        const categories: Array<any> = this.cacheService.load('categories');

        subcategories.forEach((item, index) => {
            this.requestService.activeLoader(new LoaderSemaphoreClass(1 + index));

            this.requestService.get(GET_PRODUCTS_BY_SUBCATEGORY_ID + item.Id)
                .then(response => {
                    response.responseData.forEach(product => {
                        product['SubCategory'] = item;
                        product['SubCategory']['Category'] = categories.find(category => category.Id === item.CategoryId);
                    });
                    this.products = [...this.products, ...response.responseData];
                })
                .catch(() => {
                    this.alertService.error('An error occurred while loading products');
                });
        });
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description The method allows you to retrieve the list of available products retrieved via the API
     */
    get products(): Array<any> {
        return this._products;
    }

    /**
     * @description The method allows you to assign the list of available products retrieved via the API
     *
     * @param value Value of the new list of available products retrieved via the API
     */
    set products(value: Array<any>) {
        this._products = value;
    }
}
