/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../shared/theme/theme.module';
import {PagesRoutingModule} from './pages-routing.module';
import {ComponentsModule} from '../shared/components/components.module';
import {NotFoundModule} from './not-found/not-found.module';
import {CustomerModule} from './customer/customer.module';
/**
 * Import of application's components
 */
import {PagesComponent} from './pages.component';
import {BillModule} from './bill/bill.module';
import {ProductModule} from './product/product.module';
import {UserModule} from './user/user.module';


@NgModule({
    imports: [
        PagesRoutingModule,
        ThemeModule,
        ComponentsModule,
        NotFoundModule,
        CustomerModule,
        BillModule,
        ProductModule,
        UserModule
    ],
    declarations: [
        PagesComponent,
    ],
})
export class PagesModule {
}
