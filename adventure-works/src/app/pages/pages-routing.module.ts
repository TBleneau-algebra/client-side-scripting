/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {PagesComponent} from './pages.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ProductComponent} from './product/product.component';
import {ModalComponent} from '../shared/components/modal/modal.component';
import {UserComponent} from './user/user.component';
/**
 * Import of application's services
 */
import {UserGuardService} from '../shared/services/guards/user-guard.service';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [
        {
            path: '',
            redirectTo: 'customer',
            pathMatch: 'full',
        },
        {
            path: 'customer',
            loadChildren: () => import('./customer/customer.module')
                .then(m => m.CustomerModule),
        },
        {
            path: 'bill',
            loadChildren: () => import('./bill/bill.module')
                .then(m => m.BillModule),
        },
        {
            path: 'product',
            component: ProductComponent
        },
        {
            path: 'user',
            canActivate: [UserGuardService],
            component: ModalComponent,
            data: {component: UserComponent}
        },
        {
            path: '**',
            component: NotFoundComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {
}
