import {Component, EventEmitter, OnInit} from '@angular/core';
import {RequestService} from '../../shared/services/request/request.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {CacheService} from '../../shared/services/cache/cache.service';
import {ActivatedRoute} from '@angular/router';
import {POST_EDIT_USER, POST_RETRIEVE_USER} from '../../shared/data/api-path.data';
import {UserClass} from '../../shared/classes/models/user.class';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * This EventEmitter allows to send a boolean in order to close the modal
     */
    public kill: EventEmitter<boolean>;

    /**
     * @description User model used to save information
     */
    private _model: UserClass;

    /**
     * @description Constructor of LogoutComponent component
     *
     * The constructor creates an instance of the LogoutComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param cacheService A reference to the cache service of the application
     * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
     */
    constructor(private requestService: RequestService, private alertService: AlertService, private cacheService: CacheService,
                private activatedRoute: ActivatedRoute) {
        this.model = new UserClass();
        this.kill = new EventEmitter<boolean>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadUser(this.activatedRoute.snapshot.queryParamMap.get('username'));
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load a user thanks to his username
     */
    loadUser(value: string): void {
        this.requestService.post({username: value}, POST_RETRIEVE_USER)
            .then(response => {
                this.model = response.responseData;
                this.model.img = (this.model.img) ? this.model.img : '/assets/img/user.png';
            })
            .catch(error => {
                (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message)
                    : this.alertService.error('An error occurred during retrieving your user data');
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and edit a user
     */
    editUser(event: any) {
        this.requestService.post(event.model, POST_EDIT_USER)
            .then(response => {
                this.model = response.responseData;
                this.model.img = (this.model.img) ? this.model.img : '/assets/img/user.png';
                this.alertService.success('Your profile has been successfully edited!');
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to edit your profile, you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error has occurred. Try again later.');
                }
            });
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description The method allows you to retrieve the user model used to save information
     */
    get model(): UserClass {
        return this._model;
    }

    /**
     * @description The method allows you to assign the user model used to save information
     *
     * @param value Value of the user model used to save information
     */
    set model(value: UserClass) {
        this._model = value;
    }
}
