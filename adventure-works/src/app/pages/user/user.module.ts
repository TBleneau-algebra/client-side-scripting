/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of application's components
 */
import {UserComponent} from './user.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../shared/theme/theme.module';
import {ComponentsModule} from '../../shared/components/components.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        UserComponent
    ],
    exports: [
        UserComponent
    ],
    entryComponents: [
        UserComponent
    ]
})
export class UserModule {
}
