import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {RequestService} from '../../../shared/services/request/request.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {CacheService} from '../../../shared/services/cache/cache.service';
import {Router} from '@angular/router';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {GET_BILL_ITEM_BY_BILL_ID, GET_CUSTOMER_BILL_BY_CUSTOMER_ID, GET_CUSTOMERS} from '../../../shared/data/api-path.data';
import {BillClass} from '../../../shared/classes/models/bill.class';

@Component({
    selector: 'app-bill-list',
    templateUrl: './bill-list.component.html',
})
export class BillListComponent implements OnInit, OnDestroy {

    /**
     * @description Customers information retrieved via the API
     */
    private _customers: Array<any>;

    /**
     * @description Selected customer bills information retrieved via the API
     */
    private _bills: Array<BillClass>;

    /**
     * @description Constructor of BillListComponent component
     *
     * The constructor creates an instance of the BillListComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param cacheService A reference to the cache service of the application
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private requestService: RequestService, private alertService: AlertService, private cacheService: CacheService,
                private formBuilder: FormBuilder, private router: Router) {
        this.customers = [];
        this.bills = new Array<BillClass>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.loadCustomer();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the customer available
     */
    loadCustomer(): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.get(GET_CUSTOMERS)
            .then(response => {
                this.customers = response.responseData;
            })
            .catch(() => {
                this.alertService.error('An error occurred while loading customers');
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the selected customer bills available
     *
     * @param id Selected customer id
     */
    loadBill(id: number): void {
        this.bills = [];

        this.requestService.activeLoader(new LoaderSemaphoreClass(2));
        this.requestService.get(GET_CUSTOMER_BILL_BY_CUSTOMER_ID + id)
            .then(response => {
                response.responseData.forEach(item => {
                    this.requestService.get(GET_BILL_ITEM_BY_BILL_ID + item.Id)
                        .then(response => {
                            this.bills = [...this.bills, {
                                BillInformation: item,
                                Customer: this.assignUser(item),
                                Items: response.responseData
                            }];
                        })
                        .catch(() => {
                            return [];
                        });
                });
            })
            .catch(() => {
                this.alertService.error('An error occurred while loading bills');
            });
    }

    /**
     * @description This method allows to assign the Customer object to a bill according to the CustomerId property
     *
     * @param item Bill retrieved via API
     */
    private assignUser(item: any): any {
        return this.customers.find(customer => customer.Id === item.CustomerId);

    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description The method allows you to retrieve the customers information retrieved via the API
     */
    get customers(): Array<any> {
        return this._customers;
    }

    /**
     * @description The method is used to assign the customers information retrieved via the API
     *
     * @param value Value of the new customers information retrieved via the API
     */
    set customers(value: Array<any>) {
        this._customers = value;
    }

    /**
     * @description The method allows you to retrieve the selected customer bills information retrieved via the API
     */
    get bills(): Array<BillClass> {
        return this._bills;
    }

    /**
     * @description The method is used to assign the selected customer bills information retrieved via the API
     *
     * @param value Value of the new selected customer bills information retrieved via the API
     */
    set bills(value: Array<BillClass>) {
        this._bills = value;
    }
}
