/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of application's components
 */
import {BillListComponent} from './bill-list.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
import {ComponentsModule} from '../../../shared/components/components.module';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        BillListComponent,
    ],
    exports: [
        BillListComponent
    ]
})
export class BillListModule {
}
