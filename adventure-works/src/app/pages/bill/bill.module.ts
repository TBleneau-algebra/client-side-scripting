/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {BillComponent} from './bill.component';
/**
 * Import of application's modules
 */
import {BillRoutingModule} from './bill-routing.module';
import {BillListModule} from './bill-list/bill-list.module';
import {BillCreateModule} from './bill-create/bill-create.module';
import {BillEntityModule} from './bill-entity/bill-entity.module';

@NgModule({
    imports: [
        CommonModule,
        BillRoutingModule,
        BillListModule,
        BillCreateModule,
        BillEntityModule
    ],
    declarations: [
        BillComponent,
    ]
})
export class BillModule {
}
