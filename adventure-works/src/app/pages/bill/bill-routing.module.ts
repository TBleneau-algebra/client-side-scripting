/**
 * Import of Angular's modules
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
/**
 * Import of application's components
 */
import {BillComponent} from './bill.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {BillListComponent} from './bill-list/bill-list.component';
import {BillCreateComponent} from './bill-create/bill-create.component';
import {BillEntityComponent} from './bill-entity/bill-entity.component';
/**
 * Import of application's services
 */
import {PagesGuardService} from '../../shared/services/guards/pages-guard.service';

const routes: Routes = [{
    path: '',
    component: BillComponent,
    children: [
        {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
        },
        {
            path: 'list',
            component: BillListComponent
        },
        {
            path: 'create',
            component: BillCreateComponent
        },
        {
            path: ':id',
            canActivate: [PagesGuardService],
            component: BillEntityComponent
        },
        {
            path: '**',
            component: NotFoundComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class BillRoutingModule {
}
