import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestService} from '../../../shared/services/request/request.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {CacheService} from '../../../shared/services/cache/cache.service';
import {Router} from '@angular/router';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {
    GET_CUSTOMERS,
    GET_PRODUCTS_BY_SUBCATEGORY_ID,
    POST_ADD_BILL,
    POST_ADD_CREDIT_CARD,
    POST_ADD_ITEM
} from '../../../shared/data/api-path.data';
import {BillClass} from '../../../shared/classes/models/bill.class';
import {ItemClass} from '../../../shared/classes/models/item.class';
import {CreditCardClass} from '../../../shared/classes/models/credit-card.class';

@Component({
    selector: 'app-bill-create',
    templateUrl: './bill-create.component.html',
})
export class BillCreateComponent implements OnInit, OnDestroy {

    /**
     * @description User-selected products
     */
    public selectedItems: Array<ItemClass>;

    /**
     * @description Data model to be sent to the API when creating an invoice
     */
    public model: BillClass;

    /**
     * @description Customers information retrieved via the API
     */
    private _customers: Array<any>;

    /**
     * @description Sellers information retrieved via the API
     */
    private _sellers: Array<any>;

    /**
     * @description Products information retrieved via the API
     */
    private _products: Array<any>;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description This EventEmitter variable emits customized events synchronously
     * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
     *
     * In the BillCreateComponent, this EventEmitter allows the sending of a boolean to indicate that the http request to create
     * a bill has been performed
     */
    private _emitter: EventEmitter<boolean>;

    /**
     * @description Constructor of BillCreateComponent component
     *
     * The constructor creates an instance of the BillCreateComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param requestService A reference to the http request service of the application
     * @param alertService A reference to the alert service of the application
     * @param cacheService A reference to the cache service of the application
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private requestService: RequestService, private alertService: AlertService, private cacheService: CacheService,
                private formBuilder: FormBuilder, private router: Router) {
        this.products = [];
        this.customers = [];
        this.emitter = new EventEmitter<boolean>();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initModel();
        this.loadCustomer();
        this.loadProducts();

        this.formGroup = this.formBuilder.group(
            {
                billNumber: [null, Validators.compose([
                    Validators.required,
                ])],
                comment: [null, Validators.compose([])],
                customer: [null, Validators.compose([
                    Validators.required,
                ])],
                seller: [null, Validators.compose([
                    Validators.required,
                ])],
                products: [null, Validators.compose([
                    Validators.required,
                ])],
                items: [null, Validators.compose([])]
            }
        );
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description This method is used to initialize the data model to be sent to the API when creating a bill
     */
    initModel(): void {
        if (this.model) {
            delete this.model;
        }
        this.model = new BillClass();
        this.selectedItems = new Array<ItemClass>();
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the customer available
     */
    loadCustomer(): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.get(GET_CUSTOMERS)
            .then(response => {
                this.customers = response.responseData;
                this.sellers = this.cacheService.load('sellers');
            })
            .catch(() => {
                this.alertService.error('An error occurred while loading customers');
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the selected customer bills available
     */
    loadProducts(): void {
        const subcategories: Array<any> = this.cacheService.load('subcategories');
        const categories: Array<any> = this.cacheService.load('categories');

        subcategories.forEach((item, index) => {
            this.requestService.activeLoader(new LoaderSemaphoreClass(1 + index));

            this.requestService.get(GET_PRODUCTS_BY_SUBCATEGORY_ID + item.Id)
                .then(response => {
                    response.responseData.forEach(product => {
                        product['Item'] = {ProductId: (product.Id).toString(), Quantity: '1'};
                        product['SubCategory'] = item;
                        product['SubCategory']['Category'] = categories.find(category => category.Id === item.CategoryId);
                    });
                    this.products = [...this.products, ...response.responseData];
                })
                .catch(() => {
                    this.alertService.error('An error occurred while loading products');
                });
        });
    }

    /**
     * @description This method allows the user to validate the creation of the invoice, when all the required fields are filled in
     */
    create(event: any): void {
        if (event.action === 'create') {
            this.addCreditCard(event.model);
        }
    }

    /**
     * @description This method checks if a HTML element is focused
     *
     * @param value HTML element to check
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }

    /**
     * @description This method allows you to refuse any characters other than numbers
     *
     * @param event Input value
     */
    replaceNumber(event: any): void {
        event.target.value = event.target.value
            .replace(/[^\d]/g, '');

        if (!event.target.value || event.target.value === '') {
            event.target.value = 1;
        }
    }

    /**
     * @description This method allows you to assign the products selected by the user
     * There are two array of selected products because we do not want to modify the quantity directly in the first array (products)
     *
     * @param value Item selected
     */
    onItemChange(value: Array<ItemClass>): void {
        if (value.length > 0) {

            value.forEach(product => {
                if (this.model.Items.length < value.length) {
                    if (this.model.Items.find(item => item.ProductId === product.ProductId) === undefined) {
                        this.model.Items = [...this.model.Items, JSON.parse(JSON.stringify(product))];
                    }
                } else {
                    this.model.Items = this.model.Items.filter(item => item.ProductId === product.ProductId);
                }
            });
        } else {
            this.model.Items = [];
        }
    }

    /**
     * @description This method makes it possible to delete a product thanks to its identifier
     *
     * @param id Identifier of the product to be deleted
     */
    removedProducts(id: string) {
        this.model.Items = this.model.Items.filter(item => item.ProductId !== id);
        this.selectedItems = this.selectedItems.filter(item => item.ProductId !== id);
    }

    /**
     * @description This method makes it possible to search for the name of a product thanks to its identifier
     *
     * @param id Identifier of the product for which its name is being searched for
     */
    findProductName(id: string): string {
        const product: any = this.products.find(product => product.Item.ProductId === id);

        return (product) ? product.Name : 'Undefined';
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description This method calculates the total number of products on the invoice by adding the quantity of each product
     *
     * @param items Items on the bill
     */
    totalProducts(items: Array<any>) {
        let total: number = 0;

        if (items) {
            items.forEach(item => {
                total += parseInt(item.Quantity, 10);
            });
        }
        return total;
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and create a new credit card
     */
    private addCreditCard(card: CreditCardClass): void {
        this.requestService.activeLoader(new LoaderSemaphoreClass(1));

        this.requestService.post(card, POST_ADD_CREDIT_CARD)
            .then(response => {
                this.emitter.emit(true);
                this.addBillInformation(response.responseData);
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to add a new credit card you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error occurred while registering your credit card');
                }
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and create a new bill
     */
    private addBillInformation(creditCard: any): void {
        this.model.BillInformation.CreditCardId = creditCard.Id;

        this.requestService.activeLoader(new LoaderSemaphoreClass(1));
        this.requestService.post(this.model.BillInformation, POST_ADD_BILL)
            .then(response => {
                this.addBillItems(response.responseData);
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to add a new bill card you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error occurred while registering your bill');
                }
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and add items to the new created bill
     */
    private addBillItems(bill: any): void {
        this.model.Items.forEach((item, index) => {
            item.BillId = bill.Id;

            this.requestService.activeLoader(new LoaderSemaphoreClass(1 + index));
            this.requestService.post(item, POST_ADD_ITEM)
                .then(response => {
                    this.alertService.success('Your new bill has been correctly created!');
                    this.initModel();
                })
                .catch(error => {
                    if (error.responseStatus === 401) {
                        this.alertService.error('In order to add items in your bill you must be logged in');
                    } else {
                        (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                            this.alertService.error('An error occurred while registering your bill\'s items');
                    }
                });
        });

    }

    /**
     * @description The method allows you to retrieve the customers information retrieved via the API
     */
    get customers(): Array<any> {
        return this._customers;
    }

    /**
     * @description The method is used to assign the customers information retrieved via the API
     *
     * @param value Value of the new customers information retrieved via the API
     */
    set customers(value: Array<any>) {
        this._customers = value;
    }

    /**
     * @description The method allows you to retrieve the products information retrieved via the API
     */
    get products(): Array<any> {
        return this._products;
    }

    /**
     * @description The method is used to assign the products information retrieved via the API
     *
     * @param value Value of the new products information retrieved via the API
     */
    set products(value: Array<any>) {
        this._products = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }

    /**
     * @description The method allows you to retrieve the sellers information retrieved via the API
     */
    get sellers(): Array<any> {
        return this._sellers;
    }

    /**
     * @description The method is used to assign the sellers information retrieved via the API
     *
     * @param value Value of the new sellers information retrieved via the API
     */
    set sellers(value: Array<any>) {
        this._sellers = value;
    }

    /**
     * @description The method allows you to retrieve the event emitter which allows the sending of a boolean to indicate that the
     * http request to create a bill has been performed
     */
    get emitter(): EventEmitter<boolean> {
        return this._emitter;
    }

    /**
     * @description The method is used to assign the event emitter which allows the sending of a boolean to indicate that the http request
     * to create a bill has been performed
     *
     * @param value Value of the new event emitter which allows the sending of a boolean to indicate that the http request to create
     * a bill has been performed
     */
    set emitter(value: EventEmitter<boolean>) {
        this._emitter = value;
    }
}
