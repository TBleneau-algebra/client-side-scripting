/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of application's components
 */
import {BillCreateComponent} from './bill-create.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
import {ComponentsModule} from '../../../shared/components/components.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
        RouterModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        BillCreateComponent,
    ],
    exports: [
        BillCreateComponent
    ]
})
export class BillCreateModule {
}
