import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestService} from '../../../shared/services/request/request.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {CacheService} from '../../../shared/services/cache/cache.service';
import {BillClass} from '../../../shared/classes/models/bill.class';
import {map} from 'rxjs/operators';
import {LoaderSemaphoreClass} from '../../../shared/services/loader-spinner/loader-semaphore/loader-semaphore.class.';
import {GET_PRODUCTS_BY_SUBCATEGORY_ID, POST_DELETE_BILL, POST_DELETE_ITEM} from '../../../shared/data/api-path.data';

@Component({
    selector: 'app-bill-entity',
    templateUrl: './bill-entity.component.html',
})
export class BillEntityComponent implements OnInit, OnDestroy {

    /**
     * @description Data model containing the information of an invoice
     */
    private _bill: BillClass;

    /**
     * @description Products information retrieved via the API
     */
    private _products: Array<any>;

    /**
     * @description Constructor of BillEntityComponent component
     *
     * The constructor creates an instance of the BillEntityComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
     * @param requestService A reference to the http request service of the application
     * @param cacheService A reference to the cache service of the application
     * @param alertService A reference to the alert service of the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private activatedRoute: ActivatedRoute, private requestService: RequestService, private alertService: AlertService,
                private cacheService: CacheService, private router: Router) {
        this.products = [];
        this.bill = new BillClass();
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.activatedRoute.paramMap
            .pipe(map(() => window.history.state)).subscribe(value => {
            if (!value || !value.bill) {
                this.router.navigate(['/pages/bill']).then(() => {
                });
            } else {
                this.bill = value.bill;
                this.loadProducts();
            }
        });
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }

    /**
     * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
     */
    isMobile(): boolean {
        return window.innerWidth <= 768;
    }

    /**
     * @description This method calculates the total number of products on the invoice by adding the quantity of each product
     *
     * @param items Items on the bill
     */
    totalProducts(items: Array<any>) {
        let total: number = 0;

        if (items) {
            items.forEach(item => {
                total += parseInt(item.Quantity, 10);
            });
        }
        return total;
    }

    /**
     * @description This method calculates the total price on the invoice by adding the total price of each item
     *
     * @param items Items on the bill
     */
    totalPrice(items: Array<any>) {
        let total: number = 0;

        if (items) {
            items.forEach(item => {
                total += item.TotalPrice;
            });
        }
        return Math.round(total * 100) / 100;
    }

    /**
     * @description This method allows you to hide a part of the credit card number
     *
     * @param value Value of the credit card number
     */
    hideCardNumber(value: string): string {
        if (value) {
            return 'XXXXXXXXX' + value.slice(9, 12);
        }
        return null;
    }

    /**
     * @description This method allows you to assign the expiration month of the credit card
     *
     * @param value Value of the credit card expiration month
     */
    expirationMonth(value: string): string {
        if (value && parseInt(value, 10) < 10) {
            return '0' + value;
        }
        return (value) ? value : null;
    }

    /**
     * @description This method allows you to refuse any characters other than numbers
     *
     * @param event Input value
     */
    replaceNumber(event: any): void {
        event.target.value = event.target.value
            .replace(/[^\d]/g, '');

        if (!event.target.value || event.target.value === '') {
            event.target.value = 1;
        }
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and delete the selected item from the bill
     *
     * @param id Identifier of the product to be deleted
     */
    removedProducts(id: string) {
        this.requestService.post({id: id}, POST_DELETE_ITEM)
            .then(() => {
                this.alertService.success('The product has been correctly deleted from the bill');
                this.bill.Items = this.bill.Items.filter(item => item.ProductId !== id);
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to delete an item from the bill you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error occurred while deleting an item from the bill');
                }
            });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method post of the RequestService service and delete the bill
     */
    deleteBill(): void {
        this.requestService.post({id: this.bill.BillInformation.Id}, POST_DELETE_BILL)
            .then(() => {
                this.router.navigate(['/pages/bill']).then(() => {
                    this.alertService.success('The bill has been correctly deleted');
                });
            })
            .catch(error => {
                if (error.responseStatus === 401) {
                    this.alertService.error('In order to delete a bill you must be logged in');
                } else {
                    (error.responseError && error.responseError.Message) ? this.alertService.error(error.responseError.Message) :
                        this.alertService.error('An error occurred while deleting the bill');
                }
            });
    }

    /**
     * @description This method makes it possible to search for the name of a product thanks to its identifier
     *
     * @param id Identifier of the product for which its name is being searched for
     */
    findProductName(id: string): string {
        const product: any = this.products.find(product => product.Item.ProductId === id.toString());

        return (product) ? product.Name : '';
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method get of the RequestService service and load the selected customer bills available
     */
    private loadProducts(): void {
        const subcategories: Array<any> = this.cacheService.load('subcategories');
        const categories: Array<any> = this.cacheService.load('categories');

        subcategories.forEach((item, index) => {
            this.requestService.activeLoader(new LoaderSemaphoreClass(1 + index));

            this.requestService.get(GET_PRODUCTS_BY_SUBCATEGORY_ID + item.Id)
                .then(response => {
                    response.responseData.forEach(product => {
                        product['Item'] = {ProductId: (product.Id).toString(), Quantity: '1'};
                        product['SubCategory'] = item;
                        product['SubCategory']['Category'] = categories.find(category => category.Id === item.CategoryId);
                    });
                    this.products = [...this.products, ...response.responseData];
                })
                .catch(error => {
                    this.alertService.error('An error occurred while loading products');
                });
        });
    }

    /**
     * @description The method allows you to retrieve the data model containing the information of an invoice
     */
    get bill(): BillClass {
        return this._bill;
    }

    /**
     * @description The method is used to assign the data model containing the information of an invoice
     *
     * @param value Value of the new data model containing the information of an invoice
     */
    set bill(value: BillClass) {
        this._bill = value;
    }

    /**
     * @description The method allows you to retrieve the products information retrieved via the API
     */
    get products(): Array<any> {
        return this._products;
    }

    /**
     * @description The method is used to assign the products information retrieved via the API
     *
     * @param value Value of the new products information retrieved via the API
     */
    set products(value: Array<any>) {
        this._products = value;
    }
}
