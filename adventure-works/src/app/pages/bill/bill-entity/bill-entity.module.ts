/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {BillEntityComponent} from './bill-entity.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../../shared/theme/theme.module';
import {ComponentsModule} from '../../../shared/components/components.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    imports: [
        CommonModule,
        ThemeModule,
        FormsModule,
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
        ComponentsModule
    ],
    declarations: [
        BillEntityComponent
    ],
    exports: [
        BillEntityComponent
    ]
})
export class BillEntityModule {
}
