import {Component, OnDestroy} from '@angular/core';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
})
export class NotFoundComponent implements OnDestroy {

    /**
     * @description Constructor of CustomerListComponent component
     *
     * The constructor creates an instance of the CustomerListComponent component and specifies the default values
     * the input and output variables of the component.
     */
    constructor() {
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
    }
}
