/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

/**
 * Import of Nebular's modules
 */
import {NbButtonModule} from '@nebular/theme';

/**
 * Import of application's components
 */
import {NotFoundComponent} from './not-found.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NbButtonModule
    ],
    declarations: [
        NotFoundComponent,
    ]
})
export class NotFoundModule {
}
