import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService} from '../../shared/services/alert/alert.service';
import {UserClass} from '../../shared/classes/models/user.class';
import {POST_LOGIN_USER} from '../../shared/data/api-path.data';
import {UserRequestService} from '../../shared/services/request/user-request.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

    /**
     * @description User model used to save information
     */
    private _user: UserClass;

    /**
     * @description Tracks the value and validity state of a group of FormControl instances
     */
    private _formGroup: FormGroup;

    /**
     * @description Subscription object that has an unsubscribe() method, which you call to stop receiving notifications
     */
    private subscription: Subscription;

    /**
     * @description Constructor of LoginComponent component
     *
     * The constructor creates an instance of the LoginComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param alertService A reference to the alert service of the application
     * @param userRequestService A reference to the http request service used for user information in the application
     */
    constructor(private formBuilder: FormBuilder, private router: Router, private alertService: AlertService,
                private userRequestService: UserRequestService) {
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.user = new UserClass();

        this.formGroup = this.formBuilder.group(
            {
                username: [null, Validators.compose([
                    Validators.required,
                ])],
                password: [null, Validators.compose([
                    Validators.required,
                ])]
            }
        );

        this.subscription = this.userRequestService.userObservable.subscribe(value => {
            if (value) {
                this.router.navigate(['/pages']).then(() => {
                    this.alertService.success('Login successful!');
                });
            }
        });
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular destroys to initialize the view of a component.
     * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It allows to sign in a user of the application
     *
     * It calls the method Post of the RequestService service
     */
    login(): void {
        this.userRequestService.post(this.user, POST_LOGIN_USER)
            .then(response => {
                this.userRequestService.insertedUser(response.responseData.username);
                this.userRequestService.insertedToken(response.responseData.token);
            })
            .catch(error => {
                const message: string = 'An error occurred during your connection. ';

                if (error.responseError && error.responseError.Message) {
                    this.alertService.error(message + error.responseError.Message);
                } else {
                    this.alertService.error(message);
                }
            });
    }

    /**
     * @description This method checks if a HTML element is focused
     *
     * @param value HTML element to check
     */
    isFocused(value: HTMLElement): boolean {
        return document.activeElement === value;
    }

    /**
     * @description The method allows you to retrieve the user model in the LoginComponent
     */
    get user(): UserClass {
        return this._user;
    }

    /**
     * @description The method allows you to assign the user model in the LoginComponent
     *
     * @param value Value of the user model in the LoginComponent
     */
    set user(value: UserClass) {
        this._user = value;
    }

    /**
     * @description The method allows you to retrieve the variable which tracks the value and validity state of a group of
     * FormControl instances
     */
    get formGroup(): FormGroup {
        return this._formGroup;
    }

    /**
     * @description The method allows you to assign the variable which tracks the value and validity state of a group of
     * FormControl instances
     *
     * @param value Value of the variable which tracks the value and validity state of a group of FormControl instances
     */
    set formGroup(value: FormGroup) {
        this._formGroup = value;
    }
}
