import {Component, NgZone} from '@angular/core';
import {LoaderService} from '../shared/services/loader-spinner/loader.service';

@Component({
    selector: 'app-authentication',
    styleUrls: ['./authentication.component.scss'],
    template: `
        <nb-layout>
            <nb-layout-column>
                <nb-card>
                    <nb-card-body>
                        <nb-auth-block>
                            <router-outlet></router-outlet>
                        </nb-auth-block>
                    </nb-card-body>
                </nb-card>
            </nb-layout-column>
        </nb-layout>`
})
export class AuthenticationComponent {

    /**
     * @description Constructor of AuthenticationComponent component
     *
     * The constructor creates an instance of the AuthenticationComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param zone An injectable service for executing work inside or outside of the Angular zone.
     * @param loaderService A reference to the loader service of the application
     */
    constructor(private zone: NgZone, private loaderService: LoaderService) {
    }
}
