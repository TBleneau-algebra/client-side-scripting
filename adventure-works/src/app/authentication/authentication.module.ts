/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
/**
 * Import of Nebular's modules
 */
/**
 * Import of application's modules
 */
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {LoginModule} from './login/login.module';
import {ComponentsModule} from '../shared/components/components.module';
/**
 * Import of application's components
 */
import {AuthenticationComponent} from './authentication.component';
import {ThemeModule} from '../shared/theme/theme.module';
import {RegisterModule} from './register/register.module';
import {LogoutModule} from './logout/logout.module';

@NgModule({
    imports: [
        AuthenticationRoutingModule,
        ThemeModule,
        ComponentsModule,
        LoginModule,
        RegisterModule,
        LogoutModule
    ],
    declarations: [
        AuthenticationComponent
    ]
})
export class AuthenticationModule {
}
