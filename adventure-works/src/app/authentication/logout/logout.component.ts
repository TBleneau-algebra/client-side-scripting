import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserRequestService} from '../../shared/services/request/user-request.service';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
})
export class LogoutComponent implements OnInit {

    /**
     * @description Constructor of LogoutComponent component
     *
     * The constructor creates an instance of the LogoutComponent component and specifies the default values
     * the input and output variables of the component.
     *
     * @param userRequestService A reference to the http request service used for user information in the application
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private userRequestService: UserRequestService, private router: Router) {
    }

    /**
     * @description: This method is part of the component life cycle: lifecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
    }

    /**
     * @description This method logs out the user via the UserRequestService service
     */
    logout(value: boolean): void {
        if (value) {
            this.userRequestService.removedToken();
        }
        this.router.navigate(['/pages']).then();
    }
}
