/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of application's components
 */
import {LogoutComponent} from './logout.component';
/**
 * Import of application's modules
 */
import {ThemeModule} from '../../shared/theme/theme.module';
import {ComponentsModule} from '../../shared/components/components.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ThemeModule,
        ComponentsModule
    ],
    declarations: [
        LogoutComponent
    ],
    exports: [
        LogoutComponent
    ],
    entryComponents: [
        LogoutComponent
    ]
})
export class LogoutModule {
}
