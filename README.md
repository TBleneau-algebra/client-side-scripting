# Adventure Works Database Management Application

If you would like more information about the web application, follow this link :
[README.md](./adventure-works/README.md)

### Application deployment using Docker

In order to deploy the project in Docker container, simply follow some steps listed below. 

If, unfortunately, you must have "administrator" rights to perform these commands, I invite you to add your user to the docker group of your machine.

```sh
      sudo usermod -a -G docker <USER_NAME>
```

1) To do this, you will first need to build the sources of the web application.
   For reasons of performance of some machines, it is preferable not to build the source files and binaries with Docker.  
   
   To build the source files for the web application, follow the instructions in the [adventure-works](./adventure-works/README.md) folder.  
   Then to build your Docker containers, execute the following command :

    ```sh
          docker-compose build
    ```

3) Once construction has been completed, you can launch your Docker containers

    ```sh
          docker-compose up
    ```

4) To check that the docker containers are launched correctly, you can run the following command:

    ```sh
          docker ps
    ```

5) To check the running containers, you can run the following command:

    ```sh
          docker-compose stop
    ```

6) To delete the stopped containers, you can run the following command:

    ```sh
          docker container prune
    ```

7) To delete the dangling images, you can run the following command:

    ```sh
          docker image prune
    ```

8) To delete a specific image, simply list the images and delete the image using its name or identifier:

    ```sh
          docker image ls
          docker rmi <IMAGE_NAME / IMAGE_ID>
    ```

